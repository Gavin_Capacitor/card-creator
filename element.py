from abc import ABC
class Element(ABC):
    def __init__(self):
        self.location = None
        self.layoutOrder = None
        self.name = None
        self.asset = {}
        self.inclusionList = []



class Box(Element):
    def __str__(self):
        return "Box"

class TextBlock(Element):
    def __str__(self):
        return "TextBlock"

class Icon(Element):
    def __str__(self):
        return "Icon"

