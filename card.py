import requests
import pathlib
from itertools import groupby
GOOD_PULL = 200
COLOR_MODE = {"land":"L", "artifact":"A", "colorless":"C", "gold":"O"}

class Card (object):

    def __init__(self, name):
        self.name = name

        self.colorID = None
        self.colors = None

        self.textRaw = None
        self.textEdit = None

        self.flavorRaw = None
        self.flavorEdit = None

        self.typeRaw = None
        self.typeEdit = None

        self.costRaw = None
        self.costEdit = None

        self.power = None
        self.toughness = None

        self.element = None

        self.rawAttribs = None


    def grabStatistics(self):
        """
            contacts scryfall and attempts to get requested card attributes
            Success: sets rawAttribs to scryfall's json; return 1
            Fail: return -1
        """
        url = "https://api.scryfall.com/cards/named?exact={}"

        try:
            response = requests.get(url.format(self.name), timeout=5)
        except requests.exceptions.ReadTimeout:
            print("[-] Cannot reach Scryfall")
            return -1

        if(response.status_code != GOOD_PULL):
            print("[-] Card not found")
            return -1

        self.rawAttribs = response.json()

        return 1

    def enumerateCard(self):
        """
            Converts raw attribute data to class data
            Also runs series of fucntions to gather
            Success: sets various class attribtues; return 1
            Fail: return -1
        """
        if None == self.rawAttribs:
            print("[-] Card attributes not found")
            return -1

        self.name = self.rawAttribs["name"]
        self.textRaw = self.rawAttribs["oracle_text"]
        if "flavor_text" in self.rawAttribs.keys():
            self.flavorRaw = self.rawAttribs["flavor_text"]
        self.costRaw = self.rawAttribs["mana_cost"]
        self.typeRaw = self.rawAttribs["type_line"]

        if "creature" in self.typeRaw.lower():
            self.power = self.rawAttribs["power"]
            self.toughness = self.rawAttribs["toughness"]

        return 1

    def _setColorID(self):
        """
            Converts raw color information into private color identifier format
            Success: sets colorID to single letter "color code"
        """
        colors = self.rawAttribs["color"]

        if not colors:  # card is either colorless or non-color artifact
            if "land" in self.typeRaw.lower():
                self.colorID = COLOR_MODE["land"]
            elif "artifact" in self.typeRaw.lower():
                self.colorID = COLOR_MODE["artifact"]
            else:
                self.colorID = COLOR_MODE["colorless"]

        elif len(colors) > 1:
            self.colorID = COLOR_MODE["gold"]

        else:
            self.colorID = colors[0]

    def _setColors(self):
        """
            Converts raw color information into private color code
            Success: sets colors to either single letter or tuple of two letters
        """
        rawColors = self.rawAttribs["colors"]

        if not colors:  # card is either colorless or non-color artifact
            if "artifact" in self.typeRaw.lower():
                self.colors = COLOR_MODE["artifact"]
            elif "land" in self.typeRaw.lower():
                self.colors = COLOR_MODE["land"]
            else:
                self.colors = COLOR_MODE["colorless"]

        elif 1 == len(rawColors):
            self.colors = rawColors[0]

        elif 2 == len(rawColors):
            self.colors = tuple(rawColors)

        else:
            self.colors = rawColors[0]
