from PyQt5.QtWidgets import QFileDialog
from card import Card
from magicStencilParser import Stencil
import os


def loadCard(ui, sessionCard, grabbedText):
    sessionCard = Card(grabbedText)
    check = sessionCard.grabStatistics()
    if check < 0:
        print("[-] Error in Grab Stats")
    sessionCard.enumerateCard()

    ui.txtCardName.setPlainText(sessionCard.name)
    ui.txtCardMainText.setText(sessionCard.textRaw)
    ui.txtCardFlavor.setText(sessionCard.flavorRaw)
    ui.txtCardCost.setText(sessionCard.costRaw)
    ui.txtCardTypeline.setPlainText(sessionCard.typeRaw)

    if sessionCard.power:
        ui.txtCardPower.setPlainText(sessionCard.power)
    if sessionCard.toughness:
        ui.txtCardToughness.setPlainText(sessionCard.toughness)


def grabStencil(ui, sessionStencil):
        sessionStencil = Stencil()

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        defaultStencilPath = "{}/stencils".format(os.getcwd())
        stencilFile = QFileDialog.getOpenFileName(None,
                                                  "Choose a stencil file",
                                                  defaultStencilPath,
                                                  "Stencil Files (*.stn)",
                                                  options=options)
        if(stencilFile[0]):
            stencilFile = stencilFile[0]
            stencilPath = os.path.dirname(stencilFile)
            sessionStencil.loadSTEN(stencilFile)
            sessionStencil.loadAssets(stencilPath)
            labelText = "Loaded Stencil: {}".format(sessionStencil.stencilName)
            ui.lblStencilInfo.setText(labelText)
