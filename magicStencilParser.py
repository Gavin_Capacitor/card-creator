import os
import element

skiplist = ['\n', '#']


class Stencil():

    def __init__(self):
        self.stencilName = None
        self.typeBorder = element.Box()
        self.typeBorder.inclusionList = ['w', 'b', 'u', 'r', 'g',
                                         'c', 'o', 'l', 'a']
        self.scrollL = element.Box()
        self.scrollL.inclusionList = ['w', 'b', 'u', 'r', 'g',
                                      'c', 'o', 'l', 'a']
        self.scrollR = element.Box()
        self.scrollR.inclusionList = ['w', 'b', 'u', 'r', 'g',
                                      'c', 'o', 'l', 'a']
        self.cardName = element.TextBlock()
        self.cardArt = element.Icon()
        self.textBox = element.Box()
        self.cardCost = element.TextBlock()
        self.attributeDict = {'typeBorder': self.typeBorder,
                              'scrollL': self.scrollL,
                              'scrollR': self.scrollR,
                              'cardName': self.cardName,
                              'textBox': self.textBox,
                              'cardCost': self.cardCost,
                              'cardArt': self.cardArt}

    def loadSTEN(self, filename):

        with open(filename, "r") as stenFile:
            for line in stenFile:
                if line[0] in skiplist:
                    continue

                line = line.strip()
                lineItem, options = line.split("~")
                lineItem = lineItem.strip()
                options = options.strip()

                if lineItem in self.attributeDict.keys():
                    eleName = "my name is {}".format(lineItem)
                    setattr(self, "{}.name".format(lineItem), eleName)
                    self._infilOptions(lineItem, options)

                elif lineItem == 'stencilName':
                    self.stencilName = options

    def _infilOptions(self, attribute, options):
        setline = "{}.{}"
        tribline = "self.{}".format(attribute)

        subtrib = options.split(";")

        for each in subtrib:
            subattrib, value = each.split(":")
            subattrib = subattrib.strip()
            value = value.strip()
            if hasattr(self.attributeDict[attribute], subattrib):
                setattr(self.attributeDict[attribute], subattrib, value)
                print(".")

    def loadAssets(self, assetPath):

        resourcePath = "{}/resources".format(assetPath)

        if not os.path.isdir(resourcePath):
            print("{} has no \'resources\' directory.".format(assetPath))
            return

        assetDirectories = os.listdir(resourcePath)

        for assetSubDir in assetDirectories:

            if assetSubDir in self.attributeDict.keys():
                print('..')
                assetSubDirPath = resourcePath + "/" + assetSubDir
                targetAttribute = self.attributeDict[assetSubDir]
                self._enumerateDirectory(targetAttribute, assetSubDirPath)

    def _enumerateDirectory(self, attrib, dirPath):
        dirContents = os.listdir(dirPath)

        for subFile in dirContents:
            subFileMoniker = subFile.split("_")[0]

            if subFileMoniker not in attrib.inclusionList:
                print("skipping", subFile)
                continue  # dump if moniker does not match inclusion list

            subFilePath = dirPath + "/" + subFile
            attrib.asset.update({subFileMoniker: subFilePath})
