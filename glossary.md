element: one of three items that make up a card (Box, TextBlock, Icon)
characteristic: an attribute of an element
attribute: the collection of elements in a Stencil
asset: apecific element characteristic that holds files associated with the element
moniker: how an asset is referenced by a Card


stencil layout:

The pound symbol/ number sign at the beginnin will skip the line \#

each stencil line is broken out into the following fields:

'element' ~ characteristic : characteristic_value ; next_characteristic : next_characteristic_value

Stencils will have the following directory structure:

Stencil_Dir
.
.. stencil.stn
.. resources
   .
   .. stencil_attributue_name
      .
      .. attribute_asset
      .. attribute_asset


attribte_assets are labled like so:

moniker_whatever_else_you_want_to_put.ext
