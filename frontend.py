from PyQt5 import QtCore, QtGui, QtWidgets

from main_window import Ui_MainWindow
from card import Card
from magicStencilParser import Stencil
import backend


class Session (object):

    def __init__(self):
        self.ui = None
        self.card = None
        self.stencil = None

    def interactFindCard(self):
        print("Find Card pressed")
        grabbedText = self.ui.txtCardName.toPlainText()
        grabbedText = grabbedText.strip()
        if grabbedText:
            backend.loadCard(self.ui, self.card, grabbedText)


    def interactFindArt(self):
        print("Find Art pressed")

    def interactFindStencil(self):
        print("Find Stencil pressed")
        backend.grabStencil(self.ui, self.stencil)

    def interactPreview(self):
        print("Preview pressed")

    def interactSave(self):
        print("Save pressed")

if __name__ == "__main__":
    import sys

    thisSession = Session()

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    thisSession.ui = Ui_MainWindow()
    thisSession.ui.setupUi(MainWindow)

    thisSession.ui.btnFindCard.clicked.connect(thisSession.interactFindCard)
    thisSession.ui.btnFindArt.clicked.connect(thisSession.interactFindArt)
    thisSession.ui.btnFindStencil.clicked.connect(thisSession.interactFindStencil)
    thisSession.ui.btnPreview.clicked.connect(thisSession.interactPreview)
    thisSession.ui.btnSave.clicked.connect(thisSession.interactSave)


    MainWindow.show()
    sys.exit(app.exec_())
